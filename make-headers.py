#!/usr/bin/env python
import os, shutil, re, sys
from xml.dom import minidom, XMLNS_NAMESPACE
from optparse import OptionParser

XMLNS_IFACE = "http://zero-install.sourceforge.net/2004/injector/interface"
XMLNS_0COMPILE = 'http://zero-install.sourceforge.net/2006/namespaces/0compile'

version = '0.1'
parser = OptionParser(usage="usage: %prog [options]")
parser.add_option("-k", "--keep", help="keep a file we would normally delete", action='append')
parser.add_option("-V", "--version", help="display version information", action='store_true')
(options, args) = parser.parse_args()

if options.version:
	print "Make-headers (zero-install) " + version
	print "Copyright (C) 2007 Thomas Leonard"
	print "This program comes with ABSOLUTELY NO WARRANTY,"
	print "to the extent permitted by law."
	print "You may redistribute copies of this program"
	print "under the terms of the GNU General Public License."
	print "For more information about these matters, see the file named COPYING."
	sys.exit(0)

configure = os.path.join(os.environ['SRCDIR'], 'configure')
prefix = os.environ['DISTDIR']

def run(*argv):
	if os.spawnvp(os.P_WAIT, argv[0], argv):
		raise Exception("Command '%s' failed" % repr(argv))

run(configure, '--prefix', prefix, *args)
run('make', 'install')

keep = options.keep or []

os.chdir(prefix)
for path in ['bin', 'man', 'share']:
	if path not in keep and os.path.isdir(path):
		shutil.rmtree(path)

# TODO: get 0compile to tell us this instead of guessing
xml_files = [x for x in os.listdir('0install') if x.endswith('.xml')]
xml_files.remove('build-environment.xml')
assert len(xml_files) == 1, xml_files
feed_file = os.path.abspath(os.path.join('0install', xml_files[0]))

mappings = {}

os.chdir('lib')
for x in os.listdir('.'):
	if os.path.isfile(x) or os.path.islink(x):
		if re.match('^lib.*\.so[.0-9]*$', x) or \
		   re.match('^lib.*[.0-9]*\.dylib$', x) or \
		   re.match('^lib.*\.l?a$', x):
			if x.endswith('.so') and os.path.islink(x):
				target = os.readlink(x)
				mappings[x[3:-3]] = target.split('.so.', 1)[1].split('.', 1)[0]
			if x.endswith('.dylib') and os.path.islink(x):
				target = os.readlink(x)
				mappings[x[3:-6]] = target.split('.dylib.', 1)[0].split('.', 1)[0]
			os.unlink(x)
	elif os.path.isdir(x):
		if re.match('^python.*$', x):
			shutil.rmtree(x)

if mappings:
	print "Detected library major versions:", mappings
	doc = minidom.parse(feed_file)
	impls = doc.getElementsByTagNameNS(XMLNS_IFACE, "implementation")
	assert len(impls) == 1, impls
	node = impls[0]
	# Find the innermost element with some existing mappings, if any
	while not node.hasAttributeNS(XMLNS_0COMPILE, 'lib-mappings'):
		if node.parentNode.nodeName != 'group':
			break
		node = node.parentNode
	existing_mappings = node.getAttributeNS(XMLNS_0COMPILE, 'lib-mappings')
	if existing_mappings:
		print "Merging existing mappings", existing_mappings
		node.removeAttributeNS(XMLNS_0COMPILE, 'lib-mappings')
		for existing_mapping in existing_mappings.split(' '):
			key, value = existing_mapping.split(':')
			if key in mappings:
				if value != mappings[key]:
					print "WARNING: overwriting detected mapping %s -> %s with %s!" % (key, mappings[key], value)
				else:
					print "NOTE: unnecessary mapping given in feed (%s -> %s). I can work it out for myself!" % (key, value)
			mappings[key] = value
	lib_mappings = []
	for key in mappings:
		lib_mappings.append("%s:%s" % (key, mappings[key]))
	node.setAttributeNS(XMLNS_0COMPILE, 'compile:lib-mappings', ' '.join(lib_mappings))
	doc.documentElement.setAttributeNS(XMLNS_NAMESPACE, 'xmlns:compile', XMLNS_0COMPILE)
	stream = file(feed_file, 'w')
	doc.writexml(stream)
	stream.close()

os.chdir('pkgconfig')
for pc in os.listdir('.'):
	if pc.endswith('.pc'):
		data = file(pc).read().split('\n')
		for i in range(len(data)):
			if data[i].startswith('prefix='):
				data[i] = 'prefix=${pcfiledir}/../..'
		out = file(pc, 'w')
		out.write('\n'.join(data))
		out.close()
				
